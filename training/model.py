import numpy as np
import pandas as pd
import pickle

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split

# approach based on this source : https://scikit-learn.org/stable/tutorial/text_analytics/working_with_text_data.html


# importing data
df = pd.read_csv(
    "https://elo7-datasets.s3.amazonaws.com/data_scientist_position/elo7_recruitment_dataset.csv"
)

# concateneting title and tags
df_copy = df.copy()
df_copy["text"] = df_copy["concatenated_tags"] + " " + df_copy["title"]
df_copy = df_copy[df_copy["concatenated_tags"].notnull()]


# separating data into test and train datasets
x_train, x_test, y_train, y_test = train_test_split(
    df_copy["text"], df_copy["category"], test_size=0.25, random_state=0
)

# building pipeline
text_clf = Pipeline(
    [
        ("vect", CountVectorizer()),
        ("tfidf", TfidfTransformer()),
        ("clf", MultinomialNB()),
    ]
)

# training classifier
text_clf.fit(x_train, y_train)

# rating the model according to test dataset
predicted = text_clf.predict(x_test)
print("Prediction accuracy : " + np.mean(predicted == y_test))
with open("model.pkl", "wb") as f:
    pickle.dump(text_clf, f)
