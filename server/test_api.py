# pylint: disable=import-error
import app
from flask import Flask, request, json, jsonify
import unittest
import sys
import json


class AppTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_get_home(self):
        response = self.app.get("/")
        assert response.status_code == 200
        assert (
            response.json["response"]
            == "Post your Products in Json format to get a prediction"
        )

    def test_get_category(self):

        response = self.app.post("/category", data=json.dumps({"door": "closed"}))
        assert response.status_code == 200
        assert response.json["error"] == "missing products field"

        dataw = {"products": [{"tags": "empty"}, {"name": "saitama"}]}
        response = self.app.post("/category", data=json.dumps(dataw))
        assert response.status_code == 200
        assert response.json["error"] == "missing products title field"

        data_json = {
            "products": [
                {
                    "title": "Cinto de Artes Marciais",
                    "concatenated_tags": "cinto artes-marciais roupa tecido",
                },
                {"title": "Carrinho de Bebê"},
            ]
        }
        response = self.app.post("/category", data=json.dumps(data_json))
        assert response.status_code == 200
        assert len(response.json["categories"]) == 2


if __name__ == "__main__":
    unittest.main()
