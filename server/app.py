# pylint: disable=import-error
from flask import Flask, request, jsonify
import pickle
import json
import pandas as pd

app = Flask(__name__)


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    return app


# Basic Get route
@app.route("/", methods=["GET"])
def get_home():
    return jsonify(
        {"response": "Post your Products in Json format to get a prediction"}
    )


# Post route for product categorization
@app.route("/category", methods=["POST"])
def get_category():

    try:
        prodct = json.loads(request.data)
    except ValueError:
        return json.dumps({"status": 1, "info": "request failed."})

    if prodct.get("products") is None:
        return jsonify({"error": "missing products field"})

    products_list = prodct.get("products")

    for t in products_list:
        if t.get("title") is None:
            return jsonify({"error": "missing products title field"})

    with open("model.pkl", "rb") as f:
        text_clf = pickle.load(f)

    df = pd.DataFrame(products_list)
    df["text"] = df["concatenated_tags"] + " " + df["title"]
    df = df.fillna(" ")
    categories_list = text_clf.predict(df["text"])
    return jsonify({"categories": categories_list.tolist()})
